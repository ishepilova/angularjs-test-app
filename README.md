# README #

### Repository description ###

* This is an Angular demo app
* It consists of a ui-select dropdown list and a button. When an option is chosen and the button is clicked, the option name is added below.
* Before the button click nothing happens
* If no option is chosen and the button is clicked, an error message appears

### How do I get set up? ###

* Prerequisites: git, npm and bower installed on your OS

* In your command line tool run:


```
#!javascript

git clone https://ishepilova@bitbucket.org/ishepilova/angularjs-test-app.git
npm install
bower install
gulp serve
```


That should be enough. If nothing happens, check if port 8080 is busy, try to restart your browser and/or command line tool.