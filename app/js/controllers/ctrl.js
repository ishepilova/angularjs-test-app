angular.module('app')
.controller('ctrl', function($scope,optionsList){
    'use strict';

    $scope.optionsList = optionsList;
    $scope.submitFlag = false;

    $scope.showChosenItem = function(selectedValue){
    	$scope.submitFlag = true;
    	$scope.clickedItem = selectedValue;
    	return false;
    };

    $scope.onSelected = function (selectedItem) {
    	$scope.selectedValue = selectedItem;
    	return false;
	};

	$scope.resetSubmitFlag = function (){
		$scope.submitFlag = false;
    	return false;
	};

	$scope.itemGroup = function (item) {
		return item.group;
	};

});