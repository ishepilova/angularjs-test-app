angular.module('app')
.service('optionsList', function(){

var list = [
    { attributeKey: "some_key1", attributeName: "Some Key 1", group: "GroupOne" },
    { attributeKey: "some_key2", attributeName: "Some Key 2", group: "GroupOne" },
    { attributeKey: "some_key3", attributeName: "Some Key 3", group: "GroupTwo" },
    { attributeKey: "some_key4", attributeName: "Some Key 4", group: "GroupTwo" },
    { attributeKey: "some_key5", attributeName: "Some Key 5", group: "GroupTwo" }
];

return list;

});