/*global angular */

/**
 * The main Test Task app module
 *
 * @type {angular.Module}
 */
 var app = angular.module('app', ['ngRoute', 'ui.bootstrap.showErrors', 'ui.select', 'ngSanitize'])
	
	.config(function($routeProvider, $locationProvider){
    
		$routeProvider
			.when('/', {
				controller: 'ctrl',
				templateUrl: '../index.html'
			})
			.otherwise({
				redirectTo: '/'
			});
	});