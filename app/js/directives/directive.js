angular.module('app')
.directive('listView', function(){
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'js/views/view.html'
    };
});