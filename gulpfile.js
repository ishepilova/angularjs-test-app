var gulp = require('gulp');
var browserSync = require('browser-sync');
var jshint = require('gulp-jshint');

gulp.task('serve', function(){
	browserSync.init({
		notify: false,
		port: 8080,
		server: {
			baseDir: ["app"],
			routes: {
				'/bower_components':'bower_components'
			}
		}
	})

	gulp.watch(['app/**/*.*','app/*.*'])
		.on('change',browserSync.reload);
})

gulp.task('lint', function() {
  return gulp.src(['app/**/*.js','app/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});